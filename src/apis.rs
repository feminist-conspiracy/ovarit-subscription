use crate::auth::Session;
use crate::customer::Customer;
use crate::error::Result;
use axum::{extract::Extension, routing::get, Json, Router};
use chrono::{DateTime, Datelike, TimeZone, Utc};
use serde::Serialize;
use sqlx::PgPool;
use stripe::{
    Charge, Client as StripeClient, Invoice, ListCharges, ListInvoices, ListSubscriptions,
    RangeBounds, RangeQuery, Subscription, SubscriptionStatusFilter,
};
use tracing::instrument;
pub mod subscription;

#[derive(Serialize, Debug, Copy, Clone)]
pub struct Progress {
    paid: i64,
    promised: i64,
}

#[derive(Serialize)]
pub struct UserInvoice {
    status: Option<String>,
    total: Option<i64>,
    due_date: Option<DateTime<Utc>>,
}

impl From<Invoice> for UserInvoice {
    fn from(invoice: Invoice) -> Self {
        UserInvoice {
            status: invoice.status.map(|status| status.to_string()),
            total: invoice.total,
            due_date: invoice
                .due_date
                .map(|ts| chrono::DateTime::from_timestamp(ts, 0).unwrap()),
        }
    }
}

#[instrument(skip(pool, stripe))]
pub async fn invoices(
    Extension(pool): Extension<PgPool>,
    Extension(stripe): Extension<StripeClient>,
    session: Session,
) -> Result<Json<Vec<UserInvoice>>> {
    let cust = Customer::from_session(&session, &pool).await?;

    let invoices = Invoice::list(
        &stripe,
        &ListInvoices {
            customer: Some(cust.id),
            limit: Some(100),
            ..Default::default()
        },
    )
    .await?;

    Ok(Json(
        invoices
            .data
            .into_iter()
            .map(|invoice| invoice.into())
            .collect(),
    ))
}

#[instrument(skip(stripe))]
pub async fn progress(Extension(stripe): Extension<StripeClient>) -> Result<Json<Progress>> {
    let mut progress = Progress {
        paid: 0,
        promised: 0,
    };
    let list_params = ListCharges {
        limit: Some(100),
        created: Some(RangeQuery::Bounds(RangeBounds {
            gte: Some({
                let now = Utc::now();
                let now = now.naive_utc().date();
                Utc.with_ymd_and_hms(now.year(), now.month(), 1, 0, 0, 0)
                    .single()
                    .unwrap()
                    .timestamp()
            }),
            ..Default::default()
        })),
        ..Default::default()
    };
    let mut charges = Charge::list(&stripe, &list_params)
        .await
        .unwrap()
        .paginate(list_params);
    loop {
        for charge in charges.page.data.iter() {
            if !charge.paid || charge.refunded || charge.disputed {
                continue;
            }
            // If we have an invoice it's a subscription, in which case don't add it to promised to avoid double counting.
            if charge.invoice.is_none() {
                progress.promised += charge.amount;
            }
            progress.paid += charge.amount;
        }
        if !charges.page.has_more {
            break;
        }
        charges = charges.next(&stripe).await.unwrap();
    }
    let list_params = ListSubscriptions {
        status: Some(SubscriptionStatusFilter::Active),
        limit: Some(200),
        ..Default::default()
    };
    let mut subscriptions = Subscription::list(&stripe, &list_params)
        .await
        .unwrap()
        .paginate(list_params);
    loop {
        for subscription in subscriptions.page.data.iter() {
            //if subs.contains(&subscription.id) {
            //    continue;
            //};
            progress.promised += subscription
                .items
                .data
                .iter()
                .filter(|s| {
                    s.price
                        .as_ref()
                        .as_ref()
                        .and_then(|p| {
                            p.product.as_ref().as_ref().map(|product| {
                                let prod_id = product.id();
                                prod_id == "prod_LRuancmo7yoHaX" || prod_id == "prod_Ksc0DyHWT7RlUV"
                            })
                        })
                        .unwrap_or(false)
                })
                .filter_map(|s| s.price.as_ref().as_ref().and_then(|p| p.unit_amount))
                .sum::<i64>();
        }
        if !subscriptions.page.has_more {
            break;
        };
        subscriptions = subscriptions.next(&stripe).await.unwrap();
    }
    progress.paid /= 100;
    progress.promised /= 100;

    Ok(Json(progress))
}

#[instrument]
pub fn routes() -> Router {
    Router::new()
        .route("/progress", get(progress))
        .route("/invoices", get(invoices))
}

#[cfg(test)]
mod test {
    use super::*;

    #[tokio::test]
    async fn progress_test() -> Result<()> {
        dotenvy::dotenv().ok();

        let stripe = stripe::Client::new(std::env::var("STRIPE_KEY").unwrap());
        let Json(_p) = progress(Extension(stripe)).await?;

        //assert!(p.promised > 0, "Promised should always be > 0");

        Ok(())
    }
}
