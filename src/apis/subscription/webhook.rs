use crate::error::{OvaError, Result};
use crate::{auth::User, customer::Customer};
use axum::{
    extract::Extension,
    http::{HeaderMap, StatusCode},
    response::IntoResponse,
};
use chrono::Utc;
use once_cell::sync::Lazy;
use sqlx::PgPool;
use stripe::{Client, ListSubscriptions, Subscription, Webhook};
use tracing::{error, info, instrument};

static STRIPE_SIGNING_SECRET: Lazy<String> =
    Lazy::new(|| std::env::var("STRIPE_SIGNING_SECRET").expect("Need this to start"));

//#[instrument(skip(pool))]
pub async fn webhook(
    Extension(pool): Extension<PgPool>,
    Extension(client): Extension<Client>,
    headers: HeaderMap,
    body: String,
) -> Result<impl IntoResponse, StatusCode> {
    let sig = headers
        .get("Stripe-Signature")
        .ok_or(StatusCode::BAD_REQUEST)?
        .to_str()
        .map_err(|_e| StatusCode::BAD_REQUEST)?;
    let event = Webhook::construct_event(body.as_str(), sig, STRIPE_SIGNING_SECRET.as_str())
        .map_err(|_e| StatusCode::BAD_REQUEST)?;
    let _ = match event.data.object {
        stripe::EventObject::Charge(charge) => record_charge(&pool, charge)
            .await
            .map_err(|_e| StatusCode::INTERNAL_SERVER_ERROR),
        stripe::EventObject::Subscription(subscription) => {
            record_customer(&pool, event.type_, subscription, &client)
                .await
                .map_err(|_e| StatusCode::INTERNAL_SERVER_ERROR)
        }
        _ => return Ok(()),
    };
    Ok(())
}

#[instrument(skip(pool, client))]
async fn record_customer(
    pool: &sqlx::Pool<sqlx::Postgres>,
    event_type: stripe::EventType,
    subscription: stripe::Subscription,
    client: &Client,
) -> Result<()> {
    let customer = Customer::new(subscription.customer.id());
    let user = customer.get_user(pool).await.map_err(|e| {
        error!("Failed to get user: {:?}", e);
        e
    })?;
    match event_type {
        stripe::EventType::CustomerSubscriptionCreated => {
            add_subscription(pool, client, &customer, &user).await?;
        }
        stripe::EventType::CustomerSubscriptionDeleted => {
            remove_subscription(pool, client, &customer, &user).await?;
        }
        _ => return Ok(()),
    };

    Ok(())
}

#[instrument(skip(pool, client))]
async fn add_subscription(
    pool: &sqlx::Pool<sqlx::Postgres>,
    client: &Client,
    customer: &Customer,
    user: &User,
) -> Result<()> {
    let list_subs = ListSubscriptions {
        customer: Some(customer.id.clone()),
        status: Some(stripe::SubscriptionStatusFilter::Active),
        ..Default::default()
    };
    let active_subs = Subscription::list(client, &list_subs).await?;
    info!(
        "Total Count of Subs: {:?} for user {:?}",
        active_subs.data.len(),
        customer.id
    );
    if !active_subs.data.is_empty() {
        add_badge(pool, user.uid.as_str(), "subscribed").await?;
    }
    Ok(())
}

#[instrument(skip(pool, client))]
async fn remove_subscription(
    pool: &sqlx::Pool<sqlx::Postgres>,
    client: &Client,
    customer: &Customer,
    user: &User,
) -> Result<()> {
    let list_subs = ListSubscriptions {
        customer: Some(customer.id.clone()),
        status: Some(stripe::SubscriptionStatusFilter::Active),
        ..Default::default()
    };
    let active_subs = Subscription::list(client, &list_subs).await?;
    info!(
        "Total Count of Subs: {:?} for user {:?}",
        active_subs.data.len(),
        customer.id
    );
    if active_subs.data.is_empty() {
        remove_badge(pool, user.uid.as_str(), "subscribed").await?;
    }

    Ok(())
}

#[instrument(skip(pool))]
async fn remove_badge(pool: &sqlx::Pool<sqlx::Postgres>, uid: &str, trigger: &str) -> Result<()> {
    sqlx::query(
        "
                DELETE FROM user_metadata
                WHERE xid IN (SELECT xid FROM user_metadata 
                      INNER JOIN badge on badge.trigger = $2
                      WHERE uid = $1
                      AND key = 'badge' 
                      AND value = badge.bid::varchar)
            ",
    )
    .bind(uid)
    .bind(trigger)
    .execute(pool)
    .await
    .map_err(|e| {
        error!("Failed to remove badge: {:?}", e);
        e
    })?;

    Ok(())
}

#[instrument(skip(pool))]
async fn add_badge(pool: &sqlx::Pool<sqlx::Postgres>, uid: &str, trigger: &str) -> Result<()> {
    sqlx::query(
        "
                INSERT INTO user_metadata (uid, key, value)
                SELECT $1, 'badge', badge.bid::varchar FROM badge
                    WHERE NOT EXISTS (
                        SELECT 1 FROM user_metadata 
                            WHERE uid = $1 
                            AND key = 'badge'
                            AND value = badge.bid::varchar
                    )
                    AND badge.trigger = $2
            ",
    )
    .bind(uid)
    .bind(trigger)
    .execute(pool)
    .await
    .map_err(|e| {
        error!("Failed to add badge: {:?}", e);
        e
    })?;

    Ok(())
}

#[instrument(skip(pool))]
async fn record_charge(pool: &sqlx::Pool<sqlx::Postgres>, charge: stripe::Charge) -> Result<()> {
    let customer = Customer::new(
        charge
            .customer
            .ok_or_else(|| OvaError::BadInput("No Customer".to_string()))?
            .id(),
    );
    let user = customer.get_user(pool).await.map_err(|e| {
        error!("Failed to get user: {:?}", e);
        e
    })?;
    add_badge(pool, user.uid.as_str(), "donated").await?;
    insert_into_payments(
        charge.id.as_str(),
        user.uid.as_str(),
        charge.status.as_str() == "succeeded" && !charge.refunded,
        charge.amount_captured / 100,
        pool,
    )
    .await?;

    Ok(())
}

#[instrument(skip(pool))]
async fn insert_into_payments(
    payment_id: &str,
    uid: &str,
    paid: bool,
    amount: i64,
    pool: &sqlx::Pool<sqlx::Postgres>,
) -> Result<()> {
    sqlx::query(
        "
        INSERT INTO payments(payment_id, uid, paid, amount, date)
        VALUES($1, $2, $3, $4, $5)
        ON CONFLICT (payment_id) 
        DO 
            UPDATE SET paid = $3
    ",
    )
    .bind(payment_id)
    .bind(uid)
    .bind(paid)
    .bind(amount)
    .bind(Utc::now())
    .execute(pool)
    .await
    .map_err(|e| {
        error!("Failed to insert payment: {:?}", e);
        e
    })?;

    Ok(())
}
