use crate::auth::Session;
use crate::customer::Customer;
use crate::error::{OvaError, Result};
use crate::price::Price;
use axum::extract::Query;
use axum::response::IntoResponse;
use axum::{
    extract::Extension,
    routing::{get, post},
    Json, Router,
};
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use sqlx::PgPool;
use stripe::{
    CheckoutSession, CheckoutSessionMode, Client as StripeClient, CreateCheckoutSession,
    CreateCheckoutSessionLineItems, ListSubscriptions, Subscription, SubscriptionStatusFilter,
    UpdateSubscription,
};
use tokio::join;
use tracing::instrument;

mod webhook;

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PaymentRequest {
    amount: u32,
    sub: bool,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct SubState {
    start: DateTime<Utc>,
    end: Option<DateTime<Utc>>,
    next: Option<DateTime<Utc>>,
    amount: u32,
    expiration: Option<String>,
    last_four: Option<String>,
}

impl From<Subscription> for SubState {
    fn from(sub: Subscription) -> SubState {
        SubState {
            start: DateTime::from_timestamp(sub.start_date, 0).unwrap(),
            end: sub
                .cancel_at
                .map(|at| DateTime::from_timestamp(at, 0).unwrap()),
            // This should be None if end and next are equal...
            next: if sub.cancel_at.unwrap_or(i64::MAX) >= sub.current_period_end {
                DateTime::from_timestamp(sub.current_period_end, 0)
            } else {
                None
            },
            amount: sub
                .items
                .data
                .into_iter()
                .filter_map(|item| Some(((item.price)?.unit_amount)? as u32))
                .sum::<u32>()
                / 100,
            expiration: sub
                .default_payment_method
                .clone()
                .and_then(|payment_method| payment_method.into_object())
                .and_then(|payment_method| payment_method.card)
                .map(|card| format!("{}/{}", card.exp_month, card.exp_year)),
            last_four: sub
                .default_payment_method
                .and_then(|payment_method| payment_method.into_object())
                .and_then(|payment_method| payment_method.card)
                .map(|card| card.last4),
        }
    }
}

#[instrument(skip(pool, stripe))]
pub async fn setup_session(
    Extension(pool): Extension<PgPool>,
    Extension(stripe): Extension<StripeClient>,
    session: Session,
    Json(payment): Json<PaymentRequest>,
) -> Result<Json<Value>> {
    let (cust, price) = join!(
        Customer::get_or_create(&session, &pool, &stripe),
        Price::get_or_create(payment.amount, payment.sub, &stripe),
    );
    let (customer, price) = (cust?, price?);

    let session = stripe::CheckoutSession::create(
        &stripe,
        CreateCheckoutSession {
            customer: Some(customer.id),
            mode: Some(if payment.sub {
                CheckoutSessionMode::Subscription
            } else {
                CheckoutSessionMode::Payment
            }),
            line_items: Some(Vec::from([CreateCheckoutSessionLineItems {
                adjustable_quantity: None,
                //description: if payment.sub {
                //    Some("Subscription to Ovarit".to_string()).into()
                //} else {
                //    Some("One Time Payment to Ovarit".to_string()).into()
                //},
                dynamic_tax_rates: None,
                price: Some(price.id.to_string()),
                price_data: None,
                quantity: Some(1),
                tax_rates: None,
            }])),
            cancel_url: Some(&std::env::var("STRIPE_CANCEL_URL").expect("STRIPE_CANCEL_URL")),
            ..CreateCheckoutSession::new(
                &std::env::var("STRIPE_SUCCESS_URL").expect("STRIPE_SUCCESS_URL"),
            )
        },
    )
    .await?;

    Ok(Json(json!({
        "url": session.url
    })))
}

#[derive(Debug, Deserialize)]
pub struct SessionQuery {
    session_id: String,
}

#[instrument(skip(pool, stripe))]
pub async fn get_session(
    Extension(pool): Extension<PgPool>,
    Extension(stripe): Extension<StripeClient>,
    Query(query): Query<SessionQuery>,
    session: Session,
) -> Result<Json<Value>> {
    let checkout_session: CheckoutSession = stripe
        .get_query(
            format!("/checkout/sessions/{}", query.session_id).as_str(),
            json!({"expand": ["line_items"]}),
        )
        .await?;

    let cust = checkout_session
        .customer
        .ok_or_else(|| OvaError::BadInput("Session had no customer".to_string()))?;
    let internal = Customer::get_or_create(&session, &pool, &stripe).await?;
    if internal.id != cust.id() {
        return Err(OvaError::NoSuchSubscription);
    }

    let amount = checkout_session
        .amount_total
        .ok_or_else(|| OvaError::BadInput("Session had no total".to_string()))?;
    let sub = checkout_session
        .line_items
        .data
        .iter()
        .any(|item| item.price.iter().any(|p| p.recurring.is_some()));

    Ok(Json(json!({
        "amount": amount / 100,
        "sub": sub
    })))
}

#[instrument(skip(pool, stripe))]
pub async fn subsciption(
    Extension(pool): Extension<PgPool>,
    Extension(stripe): Extension<StripeClient>,
    session: Session,
) -> Result<Json<SubState>> {
    let cust = Customer::from_session(&session, &pool).await?;

    let list = ListSubscriptions {
        customer: Some(cust.id),
        status: Some(SubscriptionStatusFilter::Active),
        expand: &["data.default_payment_method"],
        ..Default::default()
    };

    let subs = Subscription::list(&stripe, &list).await?;

    Ok(Json(
        subs.data
            .first()
            .ok_or(OvaError::NoSuchSubscription)?
            .to_owned()
            .into(),
    ))
}

#[instrument(skip(pool, stripe))]
pub async fn unsubscribe(
    Extension(pool): Extension<PgPool>,
    Extension(stripe): Extension<StripeClient>,
    session: Session,
) -> Result<Json<SubState>> {
    let cust = Customer::from_session(&session, &pool).await?;

    let list = ListSubscriptions {
        customer: Some(cust.id.clone()),
        status: Some(SubscriptionStatusFilter::Active),
        ..Default::default()
    };

    let subs = Subscription::list(&stripe, &list).await?;
    let sub = subs.data.first().ok_or(OvaError::NoSuchSubscription)?;

    let update = Subscription::update(
        &stripe,
        &sub.id,
        UpdateSubscription {
            cancel_at_period_end: Some(true),
            ..Default::default()
        },
    )
    .await?;

    Ok(Json(update.into()))
}

#[instrument(skip(pool, stripe))]
pub async fn update_user(
    Extension(pool): Extension<PgPool>,
    Extension(stripe): Extension<StripeClient>,
    session: Session,
) -> Result<impl IntoResponse> {
    Customer::sync_from_throat(&session, &pool, &stripe).await?;

    Ok(())
}

#[instrument()]
pub fn routes() -> Router {
    Router::new()
        .route(
            "/subscription",
            get(subsciption).post(setup_session).delete(unsubscribe),
        )
        .route("/subscription/checkout", get(get_session))
        .route("/subscription/user", post(update_user))
        .route("/subscription/stripe_webhook", post(webhook::webhook))
}
