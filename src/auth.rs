use crate::error::{OvaError, Result};
use axum::{
    extract::{Extension, FromRequestParts},
    http::{request::Parts, StatusCode},
};
use base64::Engine;
use hmac::{Mac, SimpleHmac};
use libflate::zlib::Decoder;
use once_cell::sync::Lazy;
use serde::{Deserialize, Serialize};
use sha1::Sha1;
use sqlx::{query, PgPool, Row};
use std::{
    fmt::Debug,
    io::Read,
    time::{Duration, SystemTime},
};
use tower_cookies::Cookies;
use tracing::{error, instrument};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Session {
    pub _fresh: bool,
    pub _id: Option<String>,
    pub _user_id: String,
    pub csrf_token: String,
    pub language: Option<String>,
    #[serde(default)]
    pub remember_me: bool,
}

#[derive(Debug, Deserialize, Clone)]
pub struct User {
    pub uid: String,
    pub name: String,
    pub email: String,
}

type HmacSha1 = SimpleHmac<Sha1>;

static SESSIONS_SIGNING_KEY: Lazy<Vec<u8>> = Lazy::new(|| {
    let secret_key = std::env::var("APP_SECRET_KEY").expect("Need this to start");
    let mut mac = HmacSha1::new_from_slice(secret_key.trim().as_bytes()).expect("");
    mac.update(b"cookie-session");
    let result = mac.finalize();
    result.into_bytes().to_vec()
});

impl Session {
    #[instrument(skip(pool))]
    pub async fn get_user(&self, pool: &PgPool) -> Result<User> {
        let user = sqlx::query(
            "
            select u.name, ul.email, u.uid from public.user as u
            JOIN user_login as ul on ul.uid = u.uid
            where uid = $1
            ",
        )
        .bind(&self._user_id)
        .fetch_one(pool)
        .await
        .map_err(OvaError::DatabaseQueryIssue)?;

        let name: Option<String> = user.get("name");
        let email: Option<String> = user.get("email");

        Ok(User {
            uid: user.get("uid"),
            email: email.ok_or(OvaError::CorruptDatabase)?,
            name: name.ok_or(OvaError::CorruptDatabase)?,
        })
    }

    #[instrument(skip(pool))]
    pub async fn get_metadata(&self, key: &str, pool: &PgPool) -> Result<Vec<String>> {
        query("SELECT value from user_metadata where uid = $1 and key = $2 ")
            .bind(&self._user_id)
            .bind(key)
            .fetch_all(pool)
            .await
            .map_err(OvaError::DatabaseQueryIssue)
            .map(|records| {
                records
                    .into_iter()
                    .filter_map(|record| record.get("value"))
                    .collect()
            })
    }

    #[allow(unused)]
    #[instrument(skip(pool))]
    pub async fn add_metadata(&self, key: &str, value: &str, pool: &PgPool) -> Result<()> {
        query(
            "INSERT INTO user_metadata (uid, key, value)
                VALUES ($1, $2, $3)
            ",
        )
        .bind(self._user_id.as_str())
        .bind(key)
        .bind(value)
        .execute(pool)
        .await
        .map_err(OvaError::DatabaseQueryIssue)
        .map(|_| ())
    }

    #[instrument(skip(pool))]
    pub async fn cond_set_metadata(&self, key: &str, value: &str, pool: &PgPool) -> Result<()> {
        query(
            "INSERT INTO user_metadata (uid, key, value)
                SELECT $1, $2, $3
                WHERE NOT EXISTS (
                    SELECT 1 FROM user_metadata WHERE uid = $4 AND key = $5
                )
            ",
        )
        .bind(&self._user_id)
        .bind(key)
        .bind(value)
        .bind(&self._user_id)
        .bind(key)
        .execute(pool)
        .await
        .map_err(OvaError::DatabaseQueryIssue)
        .map(|_| ())
    }
}

impl<S> FromRequestParts<S> for Session
where
    S: Send + std::marker::Sync,
{
    type Rejection = (StatusCode, &'static str);

    #[instrument(skip(request, state))]
    async fn from_request_parts(request: &mut Parts, state: &S) -> Result<Self, Self::Rejection> {
        let Extension(cookies): Extension<Cookies> = Extension::from_request_parts(request, state)
            .await
            .map_err(|e| {
                error!("Failed to get cookie layer: {:?}", e);
                (StatusCode::UNAUTHORIZED, "No cookie")
            })?;
        // We are ignoring "Remember Me" since we just want to deal with sessions and sessions
        // should be updated by other parts of the system.
        cookies
            .get("session")
            .ok_or_else(|| {
                error!("Didn't get the session token out of the cookie");
                (StatusCode::UNAUTHORIZED, "No cookie")
            })
            .and_then(|cookie| {
                let jws = cookie.value().as_bytes();
                decode_and_check(jws, ONE_MONTH)
            })
    }
}

type TokenError = (StatusCode, &'static str);
type TokenResult<S> = Result<S, TokenError>;
static ONE_MONTH: u64 = 2678400;

fn decode_and_check(jws: &[u8], time_padding: u64) -> TokenResult<Session> {
    let mut parter = jws.rsplitn(2, |v| v == &b'.');
    let sig = parter.next().ok_or_else(|| bad_format("Expected a sig"))?;
    let signed = parter
        .next()
        .ok_or_else(|| bad_format("Expected rest of value"))?;

    check_sig(decode(sig)?.as_slice(), signed)?;
    let mut parter = signed.rsplitn(2, |v| v == &b'.');
    let encoded_timestamp = parter
        .next()
        .ok_or_else(|| bad_format("Expected a timestamp"))?;
    check_timestamp(decode(encoded_timestamp)?.as_slice(), time_padding)?;

    let val = parter
        .next()
        .ok_or_else(|| bad_format("Expected a value"))?;

    decode_data(val)
}

fn check_timestamp(encoded_timestamp: &[u8], time_padding: u64) -> TokenResult<()> {
    let timestamp = if encoded_timestamp.len() == 4 {
        // 32 bit number out of Python
        u32::from_be_bytes(encoded_timestamp.try_into().map_err(bad_format)?) as u64
    } else {
        u64::from_be_bytes(encoded_timestamp.try_into().map_err(bad_format)?)
    };

    if SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .expect("We can't be before EPOCH")
        > Duration::from_secs(timestamp + time_padding)
    {
        error!(
            "Timestamp too old: {:?} + padding {:?} = {:?}",
            timestamp,
            time_padding,
            timestamp + time_padding
        );
        Err((StatusCode::UNAUTHORIZED, "Token too old"))
    } else {
        Ok(())
    }
}

fn check_sig(sig: &[u8], parts: &[u8]) -> TokenResult<()> {
    let bytes = sign(parts);
    if bytes.as_slice() != sig {
        error!("input sig didn't match calculated sig");
        Err((StatusCode::UNAUTHORIZED, "Bad Signature"))
    } else {
        Ok(())
    }
}

pub fn sign(parts: &[u8]) -> Vec<u8> {
    let mut mac = HmacSha1::new_from_slice(SESSIONS_SIGNING_KEY.as_slice())
        .expect("Works with variable sized keys");
    mac.update(parts);
    let result = mac.finalize();
    result.into_bytes().to_vec()
}

fn bad_format<T: Debug>(e: T) -> TokenError {
    error!("Poorly formatted session token: {:?}", e);
    (StatusCode::UNAUTHORIZED, "Improper Format")
}

const URL_SAFE_ENGINE: base64::engine::GeneralPurpose = base64::engine::GeneralPurpose::new(
    &base64::alphabet::URL_SAFE,
    base64::engine::general_purpose::NO_PAD,
);

fn decode(input: &[u8]) -> TokenResult<Vec<u8>> {
    let mut compressed = Vec::new();
    URL_SAFE_ENGINE
        .decode_vec(input, &mut compressed)
        .map_err(bad_format)?;

    Ok(compressed)
}

fn decode_data(val: &[u8]) -> TokenResult<Session> {
    let zip = val[0] == b'.';
    let val = if zip { &val[1..] } else { val };
    let mut decoded_data = Vec::new();
    let val = if zip {
        let val = decode(val)?;
        let mut unzip = Decoder::new(val.as_slice()).map_err(bad_format)?;
        unzip.read_to_end(&mut decoded_data).map_err(bad_format)?;
        decoded_data.as_slice()
    } else {
        decoded_data = decode(val)?;
        decoded_data.as_slice()
    };

    let mut session: Session = serde_json::from_slice(val).map_err(bad_format)?;

    if let Some((id, _reset_count)) = session._user_id.split_once('$') {
        session._user_id = id.to_string();
    };

    Ok(session)
}

#[cfg(test)]
mod test {
    use super::*;

    // Timestamp stored in this value is: 1607479410 as a 32 bit int
    static TOKEN: &str = r#".eJwlj0tqxEAMRO9isoxB_VFLPZcxklqahIwnYI9XIXdPQ5YqeFVPP8sWh58fy-11XP6-bJ9juS0E0BvrkBrGmRUoWMaAzOYlHJ0JvecyGtCQUTywtZLINEtOuXpTbUmQMxYRGFwDayGKZqLcu5dqpVkU6aVrQC2dUI2DyqxXW6bIdfrxbwMJTQbjakNsrdVh7dBoFU0dO9UBGG9pMnYesb2-v_w5qamRlCpK5ywBEhQJVHOab6lb5qmfcdDkHvK8X3L3Sc3r8N13neP7DEIep__-AX88VTU.X9Awcg.5CCPisQH8Y0XwsidMCzL_z_uYUY"#;

    #[test]
    fn decode_cookie() -> TokenResult<()> {
        // TODO: Audit that the environment access only happens in single-threaded code.
        unsafe { std::env::set_var("APP_SECRET_KEY", "super secret key") };

        decode_and_check(
            TOKEN.as_bytes(),
            SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .unwrap()
                .as_secs(),
        )?;

        Ok(())
    }

    #[test]
    #[ignore]
    fn real_life() -> TokenResult<()> {
        let res = decode_and_check("eyJfZnJlc2giOmZhbHNlLCJjc3JmX3Rva2VuIjoiNjQ3YTg5Nzk0ZWNiZTE1OGViZDAzMTQwN2YxMzdlNzNjZDBjN2ZkYSIsImxhbmd1YWdlIjoiIiwiX3VzZXJfaWQiOiI3N2E2YzFhOC04M2FiLTQ3M2ItOTYwOS1iYTY1ZTcyNzc5MzgkMCJ9.YdIUlw.I7Z2csCpm-Di6cA4HPS59aJBzvM".as_bytes(), 
            SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .unwrap()
                .as_secs(),
    )?;
        println!("Session: {:?}", res);

        Ok(())
    }

    #[test]
    #[ignore = "OnceCell"]
    fn generate_secret_key() -> TokenResult<()> {
        // TODO: Audit that the environment access only happens in single-threaded code.
        unsafe { std::env::set_var("APP_SECRET_KEY", "secret-key") };

        let moodle_value: &[i8] = &[
            -57, -8, 104, -87, -91, 56, -35, 87, -109, -89, -120, -96, -3, -57, -1, -52, 112, 45,
            -37, 80,
        ];
        let u8value = unsafe { &*(moodle_value as *const _ as *const [u8]) };

        assert_eq!(SESSIONS_SIGNING_KEY.as_slice(), u8value);

        Ok(())
    }

    #[test]
    #[ignore = "OnceCell"]
    fn check_sig_value() -> TokenResult<()> {
        // TODO: Audit that the environment access only happens in single-threaded code.
        unsafe { std::env::set_var("APP_SECRET_KEY", "secret-key") };

        let moodle_value: &[i8] = &[
            1, -21, -80, 54, 68, 77, -50, 96, -30, -52, 115, 63, 44, -40, -67, -123, -107, 52, 117,
            -78,
        ];
        let u8value = unsafe { &*(moodle_value as *const _ as *const [u8]) };
        check_sig(u8value, "value".as_bytes())?;

        Ok(())
    }
}
