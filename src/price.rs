use std::collections::HashMap;

use crate::error::{OvaError, Result};
use serde::{Deserialize, Serialize};
use tracing::instrument;

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Price {
    pub amount: u32,
    pub id: stripe::PriceId,
}

static PRODUCT_CACHE: tokio::sync::Mutex<Option<stripe::Product>> =
    tokio::sync::Mutex::const_new(None);
static PRICE_CACHE: tokio::sync::Mutex<Option<HashMap<(u32, bool), Price>>> =
    tokio::sync::Mutex::const_new(None);

#[instrument(skip(stripe))]
pub async fn get_product(stripe: &stripe::Client) -> Result<stripe::Product> {
    {
        let cache = PRODUCT_CACHE.lock().await;
        if let Some(product) = &*cache {
            return Ok(product.clone());
        };
    }
    let product = stripe::Product::list(stripe, &Default::default())
        .await
        .map_err(OvaError::StripeError)?
        .data
        .into_iter()
        .find(|product| {
            (product.name)
                .as_ref()
                .map(|name| name.as_str() == "Ovarit Donation")
                .unwrap_or(false)
        })
        .ok_or(OvaError::NoSubscriptionProduct)?;
    {
        let mut cache = PRODUCT_CACHE.lock().await;
        *cache = Some(product.clone());
    }
    Ok(product)
}

impl Price {
    #[instrument(skip(stripe))]
    pub async fn get_or_create(amount: u32, sub: bool, stripe: &stripe::Client) -> Result<Price> {
        let unit_amount: Option<i64> = Some((amount * 100) as i64);
        {
            let mut cache = PRICE_CACHE.lock().await;
            match *cache {
                Some(ref map) => {
                    if let Some(price) = map.get(&(amount, sub)) {
                        return Ok(price.clone());
                    }
                }
                None => {
                    *cache = Some(HashMap::new());
                }
            }
        }
        let pid = get_product(stripe).await?;
        let product = Some(stripe::IdOrCreate::Id(pid.id.as_str()));
        let price_list_query = stripe::ListPrices {
            product: product.clone(),
            limit: Some(100),
            ..Default::default()
        };

        let price = stripe::Price::list(stripe, &price_list_query)
            .await
            .map_err(OvaError::StripeError)?
            .data
            .into_iter()
            .find(|price| {
                price.unit_amount == unit_amount
                    && ((price.recurring.is_some() && sub) || (price.recurring.is_none() && !sub))
            });
        let price = if let Some(price) = price {
            price
        } else {
            let create_price = stripe::CreatePrice {
                unit_amount,
                recurring: if sub {
                    Some(stripe::CreatePriceRecurring {
                        trial_period_days: None,
                        interval: stripe::CreatePriceRecurringInterval::Month,
                        usage_type: None,
                        interval_count: None,
                        aggregate_usage: None,
                    })
                } else {
                    None
                },
                product,
                ..stripe::CreatePrice::new(stripe::Currency::USD)
            };
            stripe::Price::create(stripe, create_price)
                .await
                .map_err(OvaError::StripeError)?
        };
        let price = Price {
            amount,
            id: price.id,
        };
        {
            let mut cache = PRICE_CACHE.lock().await;
            if let Some(ref mut map) = *cache {
                map.insert((amount, sub), price.clone());
            };
        }
        Ok(price)
    }
}
