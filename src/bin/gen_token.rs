use base64::Engine;
use chrono::Utc;
use eyre::Result;
use payments::auth::{sign, Session};

fn main() -> Result<()> {
    let session = Session {
        _fresh: true,
        _id: None,
        _user_id: "".to_string(),
        csrf_token: "".to_string(),
        language: None,
        remember_me: false,
    };

    let value = encode(serde_json::to_string(&session)?.as_bytes());
    let timestamp = encode(&Utc::now().timestamp().to_be_bytes());

    let mut encoded = format!("{}.{}", value, timestamp);
    let sig = sign(encoded.as_bytes());
    let sig = encode(sig.as_slice());
    encoded = format!("{}.{}", encoded, sig);

    println!("{}", encoded);

    Ok(())
}

const URL_SAFE_ENGINE: base64::engine::GeneralPurpose = base64::engine::GeneralPurpose::new(
    &base64::alphabet::URL_SAFE,
    base64::engine::general_purpose::NO_PAD,
);

fn encode(input: &[u8]) -> String {
    let mut output_buf = String::new();
    URL_SAFE_ENGINE.encode_string(input, &mut output_buf);

    output_buf
}
