use axum::{extract::Extension, Json};
use stripe::Client;

#[tokio::main]
async fn main() {
    use payments::apis::progress;

    let stripe_client = Extension(Client::new("sk_test_51I4D2NBO3GBeMVp1itZRwWzehN5axxosFlOJWBXZG2JuYQcverCLfY6YXE4t9FX5kfBDcI7r9uwljW6YcO8tosTz00rJnjHISP"));
    let Json(progress_value) = progress(stripe_client).await.unwrap();
    println!("{progress_value:?}");
}
