use clap::Parser;
use color_eyre::Result;
use futures::stream::FuturesUnordered;
use futures::stream::TryStreamExt;
use payments::{auth::User, customer::Customer};
use sqlx::{postgres::PgConnectOptions, ConnectOptions, PgConnection};
use std::str::FromStr;
use url::Url;

#[derive(Debug, Parser)]
struct Opts {
    #[arg(short, long)]
    stripe_secret_key: String,
    #[arg(short, long)]
    db_url: String,
    #[arg(short, long)]
    db_password: String,
    #[arg(short, long)]
    dry: bool,
}

#[tokio::main]
pub async fn main() -> Result<()> {
    color_eyre::install()?;
    let opts: Opts = Opts::parse();
    // get a connection to the database
    let mut url = Url::from_str(&opts.db_url)?;
    url.set_password(Some(opts.db_password.as_str())).unwrap();
    let mut connection = PgConnectOptions::from_url(&url)?.connect().await?;
    // get all users that have a stripe customer
    let users: Vec<User> = get_all_users(&mut connection).await?;

    let customers: Vec<Customer> = users
        .iter()
        .map(|user| async { fetch_user_from_stripe(user, &opts.stripe_secret_key).await })
        .collect::<FuturesUnordered<_>>()
        .try_collect()
        .await?;

    // update the customer in stripe with the user's latest info
    customers
        .iter()
        .map(async |customer| -> Result<()> {
            update_customer_in_stripe(customer, &opts.stripe_secret_key).await
        })
        .collect::<FuturesUnordered<_>>()
        .try_collect::<Vec<()>>()
        .await?;

    Ok(())
}

async fn fetch_user_from_stripe(_user: &User, _stripe_secret_key: &str) -> Result<Customer> {
    let _res: Result<Customer> = todo!();
}
async fn get_all_users(_connection: &mut PgConnection) -> Result<Vec<User>> {
    let _res: Result<Vec<User>> = todo!();
}
async fn update_customer_in_stripe(_customer: &Customer, _stripe_secret_key: &str) -> Result<()> {
    let _res: Result<()> = todo!();
}
