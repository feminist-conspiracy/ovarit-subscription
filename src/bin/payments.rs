#![warn(rust_2018_idioms)]

use axum::body::Body;
use axum::http::{Request, StatusCode};
use axum::routing::get;
use axum::{extract::Extension, http::Method, Router};
use eyre::{eyre, Result};
use payments::apis;
use sqlx::PgPool;
use std::{io::Write, net::SocketAddr};
use stripe::Client as StripeClient;
use tower_cookies::CookieManagerLayer;
use tower_http::cors::{AllowOrigin, CorsLayer};
use tower_http::trace::TraceLayer;
use tracing::{info_span, Span};
use tracing_bunyan_formatter::{BunyanFormattingLayer, JsonStorageLayer};
use tracing_log::LogTracer;
use tracing_subscriber::{prelude::__tracing_subscriber_SubscriberExt, EnvFilter, Registry};
use url::Url;

#[tracing::instrument()]
fn create_pool() -> Result<PgPool> {
    let host = std::env::var("DATABASE_HOST")?;
    let port = std::env::var("DATABASE_PORT")?;
    let password = std::env::var("DATABASE_PASSWORD")?;
    let user = std::env::var("DATABASE_USER")?;
    let name = std::env::var("DATABASE_NAME")?;

    let mut database = Url::parse(&format!("postgresql://{}", host))?;
    database.set_path(&name);
    database
        .set_username(&user)
        .map_err(|_| eyre!("Username not allowed"))?;
    database
        .set_port(port.parse().ok())
        .map_err(|_| eyre!("Port not allowed"))?;
    database
        .set_password(Some(&password))
        .map_err(|_| eyre!("Password not allowed"))?;

    PgPool::connect_lazy(database.as_str())
        .map_err(|e| eyre!(e).wrap_err("Failed to connect to database"))
}

#[tracing::instrument()]
fn create_stripe_client() -> Result<StripeClient> {
    Ok(stripe::Client::new(std::env::var("STRIPE_KEY")?))
}

#[tokio::main]
#[tracing::instrument()]
async fn main() {
    use tower_request_id::{RequestId, RequestIdLayer};

    LogTracer::init().unwrap();

    dotenvy::dotenv().ok();
    let formatting_layer = BunyanFormattingLayer::new("Ovarit Auth".into(), || {
        let mut stdout = std::io::stdout();
        stdout.write_all(b"\n").unwrap();
        stdout
    });
    let subscriber = Registry::default()
        .with(JsonStorageLayer)
        .with(EnvFilter::from_default_env())
        .with(formatting_layer);
    tracing::subscriber::set_global_default(subscriber).unwrap();

    let pool = create_pool().unwrap();

    let app = Router::new()
        .merge(apis::subscription::routes())
        .merge(apis::routes())
        .layer(CookieManagerLayer::new())
        .layer(
            // Let's create a tracing span for each request
            TraceLayer::new_for_http()
                .on_failure(|error, _latency, _span: &'_ Span| {
                    tracing::error!("Some Error: {:?}", error)
                })
                .make_span_with(|request: &Request<Body>| {
                    // We get the request id from the extensions
                    let request_id = request
                        .extensions()
                        .get::<RequestId>()
                        .map(ToString::to_string)
                        .unwrap_or_else(|| "unknown".into());
                    // And then we put it along with other information into the `request` span
                    info_span!(
                        "request",
                        id = %request_id,
                        method = %request.method(),
                        uri = %request.uri(),
                    )
                }),
        )
        .layer(RequestIdLayer)
        .layer(
            CorsLayer::new()
                // Choose based on environment
                .allow_origin(AllowOrigin::predicate(|header, _parts| {
                    let header_str = header
                        .to_str()
                        .expect("Should always have a string rep for origin");
                    header_str.starts_with("http://localhost")
                        || header_str == "https://ovarit.dev"
                        || header_str == "https://ovarit.com"
                }))
                .allow_methods([Method::GET, Method::POST]),
        )
        .layer(Extension(pool.clone()))
        .layer(Extension(create_stripe_client().unwrap()))
        .route("/health", get(|| async { (StatusCode::OK, "OK") }));

    serve(app, 5003).await;
}

#[tracing::instrument(skip(app))]
async fn serve(app: Router, port: u16) {
    let addr = SocketAddr::from(([0, 0, 0, 0], port));
    let listener = tokio::net::TcpListener::bind(addr).await.unwrap();
    axum::serve(listener, app).await.unwrap();
}
