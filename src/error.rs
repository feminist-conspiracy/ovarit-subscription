use axum::{http::StatusCode, response::IntoResponse, Json};
use serde_json::json;
use thiserror::Error;
use tracing::error;

pub type Result<T, E = OvaError> = std::result::Result<T, E>;

#[derive(Debug, Error)]
pub enum OvaError {
    #[error("Database is in an unexpected state")]
    CorruptDatabase,
    #[error("Throat user has no Stripe customer associated")]
    UserHasNoCustomer,
    #[error("Subscription does not exist")]
    NoSuchSubscription,
    #[error("We have misplaced our product")]
    NoSubscriptionProduct,
    #[error("Bad user input: {0}")]
    BadInput(String),
    #[error(transparent)]
    DatabaseQueryIssue(#[from] sqlx::Error),
    #[error(transparent)]
    StripeError(#[from] stripe::StripeError),
}

impl IntoResponse for OvaError {
    fn into_response(self) -> axum::response::Response {
        use eyre::Report;
        let ie = format!("{:?}", self);

        let (status, body) = match self {
            OvaError::CorruptDatabase => (
                StatusCode::INTERNAL_SERVER_ERROR,
                "Database Corrupt, please don't try again.",
            ),
            OvaError::BadInput(_) => (StatusCode::BAD_REQUEST, ie.as_str()),
            OvaError::DatabaseQueryIssue(ref e) => map_sqlx_error(e),
            OvaError::StripeError(ref e) => map_stripe_error(e),
            OvaError::UserHasNoCustomer => (
                StatusCode::PRECONDITION_REQUIRED,
                "Please call POST /customer first to create a customer for this user.",
            ),
            OvaError::NoSuchSubscription => (
                StatusCode::PRECONDITION_REQUIRED,
                "Customer doesn't have a subscription",
            ),
            OvaError::NoSubscriptionProduct => (
                StatusCode::PRECONDITION_FAILED,
                "System doesn't have a subscription product",
            ),
        };

        let report = Report::new(self);
        error!("Default error handler: {:?}", report);

        let body = Json(json!({ "error": body }));

        (status, body).into_response()
    }
}
fn map_stripe_error(e: &stripe::StripeError) -> (StatusCode, &'static str) {
    match e {
        stripe::StripeError::Stripe(_) => (StatusCode::INTERNAL_SERVER_ERROR, "Stripe Failure"),
        stripe::StripeError::JSONSerialize(_) => (
            StatusCode::INTERNAL_SERVER_ERROR,
            "Stripe Serialization Failure",
        ),
        stripe::StripeError::QueryStringSerialize(_) => (
            StatusCode::INTERNAL_SERVER_ERROR,
            "Stripe Serialization Failure",
        ),
        stripe::StripeError::UnsupportedVersion => {
            (StatusCode::INTERNAL_SERVER_ERROR, "Stripe Failure")
        }
        stripe::StripeError::ClientError(_) => (
            StatusCode::INTERNAL_SERVER_ERROR,
            "Stripe HTTP Client Failure",
        ),
        stripe::StripeError::Timeout => (
            StatusCode::GATEWAY_TIMEOUT,
            "IO Error reading response from Stripe",
        ),
    }
}

fn map_sqlx_error(e: &sqlx::Error) -> (StatusCode, &'static str) {
    match e {
        sqlx::Error::Configuration(_) => (
            StatusCode::INTERNAL_SERVER_ERROR,
            "Database Corrupt, please don't try again.",
        ),
        sqlx::Error::Database(_) => (StatusCode::SERVICE_UNAVAILABLE, "Database Unavailable"),
        sqlx::Error::Io(_) => (StatusCode::SERVICE_UNAVAILABLE, "Database Unavailable"),
        sqlx::Error::Tls(_) => (StatusCode::SERVICE_UNAVAILABLE, "Database Misconfigured"),

        sqlx::Error::Protocol(_) => (StatusCode::SERVICE_UNAVAILABLE, "Database Misconfigured"),
        sqlx::Error::RowNotFound => (StatusCode::NOT_FOUND, "Expected data not found"),

        sqlx::Error::TypeNotFound { type_name: _ } => {
            (StatusCode::INTERNAL_SERVER_ERROR, "Cannot decode type")
        }
        sqlx::Error::ColumnIndexOutOfBounds { index: _, len: _ } => {
            (StatusCode::INTERNAL_SERVER_ERROR, "Data not found")
        }
        sqlx::Error::ColumnNotFound(_) => (StatusCode::INTERNAL_SERVER_ERROR, "Data not found"),
        sqlx::Error::ColumnDecode {
            index: _,
            source: _rce,
        } => (StatusCode::INTERNAL_SERVER_ERROR, "Cannot decode type"),
        sqlx::Error::Decode(_) => (StatusCode::INTERNAL_SERVER_ERROR, "Cannot decode type"),

        sqlx::Error::PoolTimedOut => (StatusCode::SERVICE_UNAVAILABLE, "Database Unavailable"),

        sqlx::Error::PoolClosed => (StatusCode::SERVICE_UNAVAILABLE, "Database Unavailable"),

        sqlx::Error::WorkerCrashed => (StatusCode::SERVICE_UNAVAILABLE, "Database Unavailable"),

        sqlx::Error::Migrate(_) => (
            StatusCode::INTERNAL_SERVER_ERROR,
            "Need to migrate database?",
        ),
        _ => (StatusCode::INTERNAL_SERVER_ERROR, "Unknown Error from SQLx"),
    }
}
