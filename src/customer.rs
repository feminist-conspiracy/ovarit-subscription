use std::collections::HashMap;

use crate::{
    auth::{Session, User},
    error::{OvaError, Result},
};
use serde::{Deserialize, Serialize};
use sqlx::{PgPool, Row};
use tracing::instrument;

#[derive(Debug, Deserialize, Serialize)]
pub struct Customer {
    pub id: stripe::CustomerId,
}

const USER_KEY: &str = "stripe_customer";

impl Customer {
    pub fn new(id: stripe::CustomerId) -> Self {
        Customer { id }
    }
    #[instrument(skip(pool))]
    pub async fn from_session(session: &Session, pool: &PgPool) -> Result<Customer> {
        session.get_metadata(USER_KEY, pool).await.and_then(|user| {
            user.first()
                .ok_or(OvaError::UserHasNoCustomer)
                .and_then(|id| {
                    Ok(Customer::new(
                        id.parse().map_err(|_| OvaError::UserHasNoCustomer)?,
                    ))
                })
        })
    }

    #[instrument(skip(pool))]
    pub async fn record(&mut self, session: &Session, pool: &PgPool) -> Result<()> {
        session.cond_set_metadata(USER_KEY, &self.id, pool).await?;
        *self = Customer::from_session(session, pool).await?;

        Ok(())
    }

    #[instrument(skip(pool))]
    pub async fn get_user(&self, pool: &PgPool) -> Result<User> {
        let user = sqlx::query(
            "SELECT u.name, u.uid, ul.email FROM public.user as u
                 JOIN user_metadata AS um ON um.uid = u.uid
                 JOIN user_login AS ul ON ul.uid = u.uid
                 WHERE um.key = 'stripe_customer'
                 AND um.value = $1",
        )
        .bind(self.id.to_string())
        .fetch_one(pool)
        .await
        .map_err(OvaError::DatabaseQueryIssue)?;

        let name: Option<String> = user.get("name");
        let email: Option<String> = user.get("email");

        Ok(User {
            uid: user.get("uid"),
            email: email.ok_or(OvaError::CorruptDatabase)?,
            name: name.ok_or(OvaError::CorruptDatabase)?,
        })
    }

    #[instrument(skip(pool, stripe))]
    pub async fn get_or_create(
        session: &Session,
        pool: &PgPool,
        stripe: &stripe::Client,
    ) -> Result<Customer> {
        if let Ok(customer) = Self::from_session(session, pool).await {
            return Ok(customer);
        };
        let user = session.get_user(pool).await?;

        let customer = stripe::CreateCustomer {
            name: Some(&user.name),
            email: Some(&user.email),
            metadata: Some({
                let mut map = HashMap::new();
                map.insert("user_id".to_string(), session._user_id.clone());
                map
            }),

            ..Default::default()
        };
        let mut cust = stripe::Customer::create(stripe, customer)
            .await
            .map_err(OvaError::StripeError)
            .map(|new_customer| Customer::new(new_customer.id))?;

        cust.record(session, pool).await?;

        Ok(cust)
    }

    pub(crate) async fn sync_from_throat(
        session: &Session,
        pool: &sqlx::Pool<sqlx::Postgres>,
        stripe: &stripe::Client,
    ) -> Result<()> {
        let user = session.get_user(pool).await?;
        let cust = Self::from_session(session, pool).await?;

        let customer = stripe::UpdateCustomer {
            name: Some(&user.name),
            email: Some(&user.email),
            metadata: Some({
                let mut map = HashMap::new();
                map.insert("user_id".to_string(), session._user_id.clone());
                map
            }),

            ..Default::default()
        };
        stripe::Customer::update(stripe, &cust.id, customer)
            .await
            .map_err(OvaError::StripeError)
            .map(|new_customer| Customer::new(new_customer.id))?;

        Ok(())
    }
}
