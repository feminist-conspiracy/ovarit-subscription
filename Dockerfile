# Start by building the Rust code using a rust:latest image
FROM rust:1.77.0 as builder

USER root
RUN mkdir /app
WORKDIR /app

# Copy the source code into the image
COPY . .

# Build the Rust code in release mode
RUN cargo build --release --verbose

# Use the rust:slim image as the base for the final image
FROM rust:slim

# Copy the compiled binary from the builder image
COPY --from=builder /app/target/release/payments .

# Expose the port that the application runs on
EXPOSE 5003

# Run the compiled binary when the image is started
CMD ["./payments"]
